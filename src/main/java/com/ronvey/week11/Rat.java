package com.ronvey.week11;

public class Rat extends Animal implements Walkable , Swimable,Crawlable {

    public Rat(String name) {
        super(name, 4);
    }

    @Override
    public void swim() {
        System.out.println(this + " Swim.");
        
    }

    @Override
    public void walk() {
        System.out.println(this + " Walk.");
        
    }

    @Override
    public void run() {
        System.out.println(this + " Run.");
        
    }

    @Override
    public void sleep() {
        System.out.println(this + " Sleep.");
        
    }

    @Override
    public void eat() {
        System.out.println(this + " Eat.");
        
    }

    @Override
    public String toString(){
        return "Rat (" + this.getName() + ")" ;
    }

    @Override
    public void crawl() {
        System.out.println(this + " Crawl.");
        
    }
    
}
