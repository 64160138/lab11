package com.ronvey.week11;

public class Submarine extends Vehicle implements Swimable{
    public Submarine(String name,String engine){
        super(name, engine);
    }

    @Override
    public void swim() {
        System.out.println(this.toString() + " swim.");
    }

    @Override
    public String toString(){
        return "Submarine (" + this.getName() + ")";
    }
    
}
