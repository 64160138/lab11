package com.ronvey.week11;


/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        System.out.println();
        Bird birdthongchai = new Bird("Bird thongchai");
        birdthongchai.eat();
        birdthongchai.sleep();
        birdthongchai.takeoff();
        birdthongchai.fly();
        birdthongchai.landing();
        System.out.println();

        Plane F16 = new Plane("F-16", "Locked Martin");
        F16.takeoff();
        F16.fly();
        F16.landing();
        System.out.println();

        Superman Clark = new Superman("Clark");
        Clark.takeoff();
        Clark.fly();
        Clark.landing();
        Clark.eat();
        Clark.sleep();
        Clark.walk();
        Clark.run();
        Human man1 = new Human("Punyawat");
        man1.eat();
        man1.sleep();
        man1.walk();
        man1.run();
        man1.swim();
        System.out.println();

        Submarine bigT = new Submarine("bigT", "Diesel");
        Bat bat1 = new Bat("Corona");
        Snake Johnny = new Snake("Johnny");
        Rat jerry = new Rat("Jerry");
        Dog jumbo = new Dog("Jumbo");
        Cat tom = new Cat("Tom");
        Fish Mon = new Fish("Mon");
        Crocodile Khaee = new Crocodile("Khaee");
        Flyable[] flyables = {birdthongchai,F16,Clark,bat1};
        for (int i = 0; i < flyables.length; i++) {
            flyables[i].takeoff();
            flyables[i].fly();
            flyables[i].landing();
        }
        System.out.println();
        Walkable[] walkables = {birdthongchai,Clark,man1,jerry,jumbo,tom,Khaee};
        for (int i = 0; i < walkables.length; i++) {
            walkables[i].walk();
            walkables[i].run();
        }
        System.out.println();
        Swimable[] swimables = {Clark,man1,bigT,jerry,jumbo,tom,Khaee,Mon};
        for (int i = 0; i < swimables.length; i++) {
            swimables[i].swim();
        }
        System.out.println();
        Crawlable[] crawlables = {Johnny,jerry,jumbo,tom,Khaee};
        for (int i = 0; i < crawlables.length; i++) {
            crawlables[i].crawl();
        }
    }
}
