package com.ronvey.week11;

public abstract class Animal {
    private String name;
    private int numberofLeg;
    public Animal (String name,int numberofLeg){
        this.name = name;
        this.numberofLeg = numberofLeg;
    }
    public String getName(){
        return this.name;
    }

    public int getNumberofLeg(){
        return this.numberofLeg;
    }

    public void setName(String name){
        this.name = name;
    }

    public void numberofLeg(int numberofLeg){
        this.numberofLeg = numberofLeg;
    }

    @Override
    public String toString() {
        return "Animal (" + name + ")";
    }
    public abstract void sleep();
    public abstract void eat();
}
