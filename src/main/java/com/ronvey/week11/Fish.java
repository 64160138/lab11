package com.ronvey.week11;

public class Fish extends Animal implements Swimable{

    public Fish(String name) {
        super(name,0);
        //TODO Auto-generated constructor stub
    }

    @Override
    public void swim() {
        System.out.println(this + " Swim.");
        
    }

    @Override
    public void sleep() {
        System.out.println(this + " Sleep.");
        
    }

    @Override
    public void eat() {
        System.out.println(this + " Eat.");
        
    }

    @Override
    public String toString(){
        return "Fish (" + this.getName() + ")" ;
    }
}
