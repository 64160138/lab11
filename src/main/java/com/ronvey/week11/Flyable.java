package com.ronvey.week11;

public interface Flyable {
    public void takeoff();
    public void fly();
    public void landing();
}
