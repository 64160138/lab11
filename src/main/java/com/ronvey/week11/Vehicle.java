package com.ronvey.week11;

public abstract class Vehicle {
    private String name;
    private String engine;
    public Vehicle(String name, String engine){
        this.setName(name);
        this.setEngine(engine);
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public String getEngine() {
        return engine;
    }
    public void setEngine(String engine) {
        this.engine = engine;
    }
}
